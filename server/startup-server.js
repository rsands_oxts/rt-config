Meteor.startup(function() {

    // make sure there is at least one config in the system
    if ( Versions.find().count() === 0 ) {
        let setupId = Setups.insert({});
        Versions.insert({ setupId });
    }

});