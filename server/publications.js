Meteor.publish( 'setups.all',   function() {return Setups.find()} );
Meteor.publish( 'versions.all', function() {return Versions.find()} );

// // TODO: make this more efficient?
// // Meteor.publish( 'ncom.recent', function() { return Ncom.find({}, { sort: { gpsTimeMs: -1 }, limit: 100 }) } )
// Meteor.publish('ncom.recent', function() {return Ncom.find()} );