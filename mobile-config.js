// This section sets up some basic app metadata,
// the entire section is optional.
App.info({
  id: 'com.example.test.rt',
  name: 'RT Config',
  description: 'Test',
  author: 'Test',
  email: 'test@test.com',
  website: 'http://test.com'
});

// Set up resources such as icons and launch screens.
/*
App.icons({
  'iphone_2x': 'icons/icon-60x60@2x.png',
  'iphone_3x': 'icons/icon-60x60@3x.png'
});
App.launchScreens({
    // iOS
  'iphone': 'splash/splash-320x480.png',
  'iphone_2x': 'splash/splash-320x480@2x.png',
  'iphone5': 'splash/splash-320x568@2x.png',
  'iphone6': 'splash/splash-375x667@2x.png',
  'iphone6p_portrait': 'splash/splash-414x763@3x.png',
});
*/

// Set PhoneGap/Cordova preferences
App.setPreference('BackgroundColor', '0xff0000ff');
// App.setPreference('HideKeyboardFormAccessoryBar', true);
App.setPreference('Orientation', 'portrait');
App.setPreference('Orientation', 'portrait', 'ios');

App.setPreference('StatusBarOverlaysWebView', true);
App.setPreference('StatusBarStyle', 'lightcontent');

// App.setPreference('fullscreen', false);
// App.setPreference('webviewbounce', false);
// App.setPreference('UIWebViewBounce', false);
// App.setPreference('DisallowOverscroll', true);

// App.setPreference('KeyboardShrinksView', false);
// App.setPreference('DisableScrollingWhenKeyboardShrinksView', true);

App.setPreference('FadeSplashScreen', true);
App.setPreference('FadeSplashScreenDuration', '1000');

// Pass preferences for a particular PhoneGap/Cordova plugin
/*
App.configurePlugin('com.phonegap.plugins.facebookconnect', {
  APP_ID: '1234567890',
  API_KEY: 'supersecretapikey'
});
*/

// Access rules
App.accessRule('*.google.com/*');
App.accessRule('*.googleapis.com/*');
App.accessRule('*.googleusercontent.com/*');
App.accessRule('*.gstatic.com/*');
App.accessRule('*.stripe.com/*');

// Add custom tags for a particular PhoneGap/Cordova plugin
// to the end of generated config.xml.
// Universal Links is shown as an example here.
App.appendToConfig(`
  <universal-links>
    <host name="localhost:3000" />
  </universal-links>
`);