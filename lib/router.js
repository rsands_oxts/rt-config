// redirect home

FlowRouter.route('/', {
  name: 'home',
  action() { FlowRouter.go('measurementsOverview'); }
});

// config

let configRoutes = FlowRouter.group({
	prefix: '/config'
});

configRoutes.route('/overview', {
  name: 'configOverview',
  action() { BlazeLayout.render('layout', { main: 'configOverview', footer: 'configFooter' }); }
});

configRoutes.route('/device', {
  name: 'device',
  action() { BlazeLayout.render('layout', { main: 'device', footer: 'configFooter' }); }
});

configRoutes.route('/orientation', {
	name: 'orientation',
	action() { BlazeLayout.render('layout', { main: 'orientation', footer: 'configFooter', model: 'configModel' }); }
});

configRoutes.route('/position',{
	name: 'position',
	action() { BlazeLayout.render('layout', { main: 'position', footer: 'configFooter', model: 'configModel' }); }
});

// analysis

let analysisRoutes = FlowRouter.group({
  prefix: '/analysis'
});

analysisRoutes.route('/overview', {
  name: 'analysisOverview',
  action() { BlazeLayout.render('layout', { main: 'analysisOverview' }); }
});

// measurements

let measurementsRoutes = FlowRouter.group({
  prefix: '/measurements'
});

measurementsRoutes.route('/overview', {
  name: 'measurementsOverview',
  action() { BlazeLayout.render('layout', { main: 'measurementsOverview' }); }
});