/* hack for meteoric

Template.ionView.rendered = function () {                                                                              // 1
  // Reset our transition preference                                                                                   // 2
  IonNavigation.skipTransitions = false;                                                                               // 3
                                                                                                                       // 4
  // Reset our scroll position                                                                                         // 5
  var routePath = Router.current().route.path(Router.current().params);                                                // 6
  if(IonScrollPositions[routePath]) {                                                                                  // 7
    $('.overflow-scroll').not('.nav-view-leaving .overflow-scroll').scrollTop(IonScrollPositions[routePath]);          // 8
    delete IonScrollPositions[routePath];                                                                              // 9
  }                                                                                                                    // 10
};

*/    
Router = {

	current: function() {
		return {

			route: {
				path: function() {
					return null;
				}
			},

			params: function() {
				return null;
			}

		};
	}

}