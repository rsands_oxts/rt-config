Setups = new Mongo.Collection('setups');

Setups.schema = new SimpleSchema({

	// _id - automatic

	title: {
		type: String,
		optional: true
	},

	deviceType: {
		type: String,
		optional: true		// set in before.insert hook
	}

});

Setups.attachSchema(Setups.schema);

Setups.before.insert((userId, doc) => {

	doc.deviceType = "RT1003";		// TODO: get from device
	if (!doc.title) doc.title = `New ${doc.deviceType} setup`;

});

Setups.helpers({

	_createdAt() {
		// TODO: from Versions
	},

	_updatedAt() {
		// TODO: from Versions
	},

	latestVersion() {
		return Versions.findOne({setupId: this._id}, {sort: {_updatedAt: -1}});
	}
	
});