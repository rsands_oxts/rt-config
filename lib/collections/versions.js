Versions = new Mongo.Collection('versions');

Versions.schema = new SimpleSchema({

	// _id - automatic

	setupId: {
		type: String
	},

	status: {
		type: String,
		allowedValues: ['draft', 'commit'],
		defaultValue: 'draft'
	},

	config: {
		type: Object,
		blackbox: true,
		optional: true,
		// auto value set in before insert hook
	},

	_createdAt: {
		type: Date,
		optional: true
		// auto value set in before insert hook
	},

	_updatedAt: {
		type: Date,
		optional: true
		// auto value set in before insert hook
	},

});

Versions.attachSchema(Versions.schema);

Versions.before.insert((userId, doc) => {

	let d = new Date;
	doc._createdAt = d;
	doc._updatedAt = d;
	doc.config = Versions.getDefaultConfig();

});

Versions.after.update((userId, doc) => {
	doc._updatedAt = new Date;
});

Versions.getDefaultConfig = function() {

	return {
		yAxisPair: 1,			// Right / Left
		yAxisPolarity: 0,		// Right
		zAxisPair: 2,			// Down / Up
		zAxisPolarity: 0,		// Down

		xPosToFixedAxle: 0,
		yPosToCentreline: 0,
		zPosToGround: 0,

	};

};

Versions.helpers({

	staticInitAllowed() {
		// TODO
		return true;
	}

})