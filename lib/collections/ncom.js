
NcomStreamer = new Meteor.Streamer('ncom');
if (Meteor.isServer) {
    NcomStreamer.allowRead('all');
    NcomStreamer.allowWrite('all');
}