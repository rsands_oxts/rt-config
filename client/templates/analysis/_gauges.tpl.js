Template._gauges.onRendered(function() {
    let template = this;
    
	// gauges

	let options = {
		size : 130,				// Sets the size in pixels of the indicator (square)
		roll : 0,				// Roll angle in degrees for an attitude indicator
		pitch : 0,				// Pitch angle in degrees for an attitude indicator
		heading: 0,				// Heading angle in degrees for an heading indicator
		vario: 0,				// Variometer in 1000 feets/min for the variometer indicator
		airspeed: 0,			// Air speed in knots for an air speed indicator
		altitude: 0,			// Altitude in feets for an altimeter indicator
		pressure: 1000,			// Pressure in hPa for an altimeter indicator
		showBox : true,			// Sets if the outer squared box is visible or not (true or false)
		img_directory : '/jQuery-Flight-Indicators-img/'	// The directory where the images are saved to
	}

	template.airspeed = $.flightIndicator('#airspeed', 'airspeed', options);
	template.attitude = $.flightIndicator('#attitude', 'attitude', options);
    template.heading = $.flightIndicator('#heading', 'heading', options);
    
    Meteor.setInterval(function() {
		ncom = NcomService.hiresBuffer.getCurrent();

		template.heading.setHeading( ncom.heading );
		template.attitude.setPitch( ncom.pitch );
		template.attitude.setRoll( ncom.roll ); 
		// this.elements.airspeed.setAirspeed( ncom.speed ); 

    }, 20);

});