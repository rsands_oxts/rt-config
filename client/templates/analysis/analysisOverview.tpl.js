Template.analysisOverview.onCreated(function() {
	this.elements = {};
	this.ncom = new ReactiveVar(null);
});

Template.analysisOverview.onRendered(function() {

	let template = this;

	NcomService.onNcom((ncom) => {
		this.ncom.set(ncom);
		// console.log(ncom);
	});

});

Template.analysisOverview.helpers({

	ncomValue(key) {
		return Template.instance().ncom.get()
		&& Template.instance().ncom.get()[key];
	},

	ncomStatusIs(status) {
		return Template.instance().ncom.get()
		&& Template.instance().ncom.get().status === status;
	},

	ncomStatus(prop) {
		return Template.instance().ncom.get()
		&& ( Template.instance().ncom.get() &&NcomService.getNcomStatus( Template.instance().ncom.get().status ) != null ) 
		&& NcomService.getNcomStatus( Template.instance().ncom.get().status )[prop];
	},

	gnssPosMode(prop) {
		return Template.instance().ncom.get()
		&& ( NcomService.getGnssStatus( Template.instance().ncom.get().gnssPosMode ) != null ) 
		&& NcomService.getGnssStatus( Template.instance().ncom.get().gnssPosMode )[prop];
	},

	gnssVelMode(prop) {
		return Template.instance().ncom.get()
		&& ( NcomService.getGnssStatus( Template.instance().ncom.get().gnssVelMode ) != null ) 
		&& NcomService.getGnssStatus( Template.instance().ncom.get().gnssVelMode )[prop];
	},

	gnssOriMode(prop) {
		return Template.instance().ncom.get()
		&& ( NcomService.getGnssStatus( Template.instance().ncom.get().gnssOriMode ) != null ) 
		&& NcomService.getGnssStatus( Template.instance().ncom.get().gnssOriMode )[prop];
	},

	warmUpComplete() {
		let ncom = Template.instance().ncom.get(); 
		return false
	}


});

Template.analysisOverview.events({
	'click .js-status'(event, template) {
		IonPopup.alert({
			title: 'An Alert',
			template: 'This is an alert!',
			okText: 'Got It.'
		});
	}
});