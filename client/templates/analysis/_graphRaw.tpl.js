let GRAPH_TYPES = [
    [ 'Acceleration',   'Angular rate' ],
    [ 'Velocity',       'Angular rate' ],
    [ 'Position',       'Orientation' ]
];

Template._graphRaw.onCreated(function() {
    this.selectedGraphType = new ReactiveVar(0);
	this.selectedGraphDerv = new ReactiveVar(0);
	
	// this.startTimeMs = NcomService.hiresBuffer.getCurrent()._gpsTimeMs;
});

Template._graphRaw.onRendered(function() {
	let template = this;

	let ctx = document.getElementById("_graphRawCtx");
	template.lineChart = new Chart(ctx, {
	    type: 'line',
	    data: {
	        datasets: [
				{
					label: 'x',
					data: [],
					fill: false,
					pointRadius: 0,
					backgroundColor: 'transparent',
					borderColor: 'red',

				},
				{
					label: 'y',
					data: [],
					fill: false,
					pointRadius: 0,
					backgroundColor: 'transparent',
					borderColor: 'green'
				},
				{
					label: 'z',
					data: [],
					fill: false,
					pointRadius: 0,
					backgroundColor: 'transparent',
					borderColor: 'blue'
				}
			]
	    },
	    options: {
	        scales: {
	            xAxes: [{
					ticks: {
						stepSize: 1000,
						callback: function(value, index, values) {
							return '|' // value/1000;
						}
					},
					// display: false,
	                type: 'linear',
	                position: 'bottom'
				}],
				yAxes: [{
					ticks: {
						min: -20,
						max: 10	
					}
				}]
			},
			animation: {
				duration: 0
			}
		// 	legend: {
		// 		display: false
		// 	}
	    }
	});
	
	// update graph
    Meteor.setInterval(function() {
		ncomBuffer = NcomService.hiresBuffer.getAll();

		template.lineChart.data.datasets[0].data = _.map(ncomBuffer, (ncom) => {
			return { x: ncom._gpsTimeMs, y: ncom.accelX }
		});

		template.lineChart.data.datasets[1].data = _.map(ncomBuffer, (ncom) => {
			return { x: ncom._gpsTimeMs, y: ncom.accelY }
		});

		template.lineChart.data.datasets[2].data = _.map(ncomBuffer, (ncom) => {
			return { x: ncom._gpsTimeMs, y: ncom.accelZ }
		});

		template.lineChart.update();
    }, 100)
    
});

Template._graphRaw.helpers({

});