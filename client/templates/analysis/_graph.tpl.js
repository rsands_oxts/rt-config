let GRAPH_TYPES = [
    [ 'Acceleration',   'Angular rate' ],
    [ 'Velocity',       'Angular rate' ],
    [ 'Position',       'Orientation' ]
];

Template._graph.onCreated(function() {
    this.selectedGraphType = new ReactiveVar(0);
    this.selectedGraphDerv = new ReactiveVar(0);
});

Template._graph.onRendered(function() {

	let ctx = document.getElementById("_graphCtx");
	this.elements.scatterChart = new Chart(ctx, {
	    type: 'line',
	    data: {
	        datasets: [{
	            label: 'Scatter Dataset',
	            data: [{
	                x: -10,
	                y: 0
	            }, {
	                x: 0,
	                y: 10
	            }, {
	                x: 10,
	                y: 5
	            }]
	        }]
	    },
	    options: {
	        scales: {
	            xAxes: [{
	                type: 'linear',
	                position: 'bottom'
	            }]
			},
			legend: {
				display: false
			}
	    }
    });
    
});

Template._graph.helpers({

});