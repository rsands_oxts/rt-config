Template.position.events({

	'slider-scroll #xPosToFixedAxle': function(event, template, value) {
		setConfig('xPosToFixedAxle', value);
	},

	'slider-scroll #yPosToCentreline': function(event, template, value) {
		setConfig('yPosToCentreline', value);
	},

	'slider-scroll #zPosToGround': function(event, template, value) {
		setConfig('zPosToGround', value);
	}

});