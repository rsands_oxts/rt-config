Template.configOverview.helpers({

    orientationSummary() {
        return `Y axis points ${ORIENTATION_PAIRS[getConfig('yAxisPair')][getConfig('yAxisPolarity')]}, Z axis points ${ORIENTATION_PAIRS[getConfig('zAxisPair')][getConfig('zAxisPolarity')]}`
    },

    positionSummary() {
        let x = getConfig('xPosToFixedAxle' );
        let y = getConfig('yPosToCentreline');
        let z = getConfig('zPosToGround'    );
        return (
              `${x.toFixed(1)}m ${ (x>0) ? 'ahead' : 'behind'  }, ` //fixed axle, `
            + `${y.toFixed(1)}m ${ (y>0) ? 'right' : 'left' }, ` //centreline, `
            + `${z.toFixed(1)}m ${ (z>0) ? 'above   ' : 'below'   }  ` //ground. `
        )
    }

});