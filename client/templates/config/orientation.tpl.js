let ClosestOrientationTemplate = (template) => template || Template.instance().closest('orientation');

ORIENTATION_PAIRS = [
	['Forward', 'Backward'],
	['Right', 'Left'],
	['Down', 'Up']
];


Template.orientation.onCreated(function() {

});

Template.orientation.helpers({

	orientationPairs: () => ORIENTATION_PAIRS,

	selected: (axisRt, axis, polarity) => {
		// let config = ClosestOrientationTemplate().data.config;

		let axisKey = (axisRt == "y") ? 'yAxisPair' : 'zAxisPair';
		let polarityKey = (axisRt == "y") ? 'yAxisPolarity' : 'zAxisPolarity';

		return getConfig(axisKey) === axis && getConfig(polarityKey) === polarity;
	},

	axisSelected: (axisRt, axis) => {
		// let config = ClosestOrientationTemplate().data.config;

		let axisKey = (axisRt == "y") ? 'yAxisPair' : 'zAxisPair';
		return getConfig(axisKey) === axis;
	}

});

Template.orientation.events({

	'click .js-orientation': function(event, template) {
		let axisKey = (event.currentTarget.dataset.axisRt == "y") ? 'yAxisPair' : 'zAxisPair';
		let polarityKey = (event.currentTarget.dataset.axisRt == "y") ? 'yAxisPolarity' : 'zAxisPolarity';

		let axis = parseInt(event.currentTarget.dataset.axis);
		let polarity = parseInt(event.currentTarget.dataset.polarity);

		// unset if already set
		if ( getConfig(axisKey) === axis && getConfig(polarityKey) === polarity ) {
			setConfig(axisKey, null);
			setConfig(polarityKey, null);
		// otherwise set
		} else {
			setConfig(axisKey, axis);
			setConfig(polarityKey, polarity);
		}

	}

})