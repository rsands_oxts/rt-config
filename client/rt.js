// functions for listing RTs on network (stubs)

let DUMMY_RTS = [
	{
		serial: "DU022",
		ip: "192.168.20.22"
	}, 
	{
		serial: "DU053",
		ip: "192.168.20.53"
	}
];

RT = {

	getDevices: () => DUMMY_RTS,

	getDevice: (serial) => _.findWhere(DUMMY_RTS, {serial: serial})

}