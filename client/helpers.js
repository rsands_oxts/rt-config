Template.registerHelper('displayVector', function(value, dp, positiveStr, negativeStr, zeroStr) {
	positiveStr = positiveStr || "";
	valueStr = value.toFixed(dp);

	if (value > 0)
		return `${valueStr}${positiveStr}`;
	else if (value < 0)
		return (negativeStr) ? `${-valueStr}${negativeStr}` : `${valueStr}${positiveStr}`;
	else
		return (zeroStr) ? `${zeroStr}` : `${valueStr}${positiveStr}`;

});