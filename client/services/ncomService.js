import NCOM from 'ncom';        // OxTS NCOM decoder

NcomService = (new (function() {

    let NCOM_STATUSES = [
        { display: "Invalid",               info: "" },
        { display: "Raw IMU measurements",  info: "Accelerations and angular rates are valid, but they are not calibrated or to any specification." },
        { display: "Ready to initialise",   info: "Accelerations and angular rates are output 1 s in arrears. Approximate (very inaccurate) position, velocity and angles may be output." },
        { display: "Locking",               info: "Outputs are not real-time, but all fields are valid." },
        { display: "Real-time",             info: "Outputting real-time data with the specified latency guaranteed." }
    ];

    // GNSS_STATUSES defined in separate file

    this.hiresBuffer = new CyclicBuffer(400);     // 4 seconds of data at 100Hz
    this.loresBuffer = new CyclicBuffer(7200);     // 2 hours of data at 1Hz

    this.getNcomStatus = (index) => NCOM_STATUSES[index];
    this.getGnssStatus = (index) => GNSS_STATUSES[index];

    let onNcom = null;
    this.onNcom = (cb) => { onNcom = cb };

    // monitor for changes

    let ncomCounter = 0;
    NcomStreamer.on('packet', function(ncom) {
        NcomService.hiresBuffer.add(ncom);

        let all = NcomService.hiresBuffer.getAll();
        // console.log( all[0]._gpsTimeMs, all[0].gpsTimeMin, all[0].gpsTimeMsIntoMin, all[all.length-1]._gpsTimeMs, all[all.length-1].gpsTimeMin, all[all.length-1].gpsTimeMsIntoMin, (all[all.length-1]._gpsTimeMs - all[0]._gpsTimeMs) );
        
        if (ncomCounter === 0)
            NcomService.loresBuffer.add(ncom);

        if (++ncomCounter >= 100) 
            ncomCounter = 0;

        if (onNcom) onNcom(ncom);
    });

    // websocket
    NCOM.ws.listen('ws://192.168.25.79/ncom', (packet) => {
        let ncom = NCOM.ncom.decodePacket(packet);
        console.log(ncom);
    });

    this.Measurements = [
        { text: "Accelerations", type: 'graph', ncomKeys: [ 'accelX', 'accelY', 'accelZ' ], ncomColours: ['red', 'green', 'blue'] },
        { text: "Angular rates", type: 'graph', ncomKeys: [ 'rateX', 'rateY', 'rateZ' ],    ncomColours: ['red', 'green', 'blue'] }
    ]

}));