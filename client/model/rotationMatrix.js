let timer;

rotateSceneToOrientate = function(args) {
	if (timer) clearInterval(timer);

	// if one axis is not set, just rotate around the set axis
	if ( args.yAxisPair == null || args.yAxisPolarity == null ) {
		timer = setInterval(function() {
			args.scene.rotation.z += 0.01;
		}, 10);
		return;
	}
	if ( args.zAxisPair == null || args.zAxisPolarity == null ) {
		timer = setInterval(function() {
			args.scene.rotation.y += 0.01;
		}, 10);
		return;
	}


	// both axis set

	let r = ROT_MAT[args.yAxisPair][args.yAxisPolarity][args.zAxisPair][args.zAxisPolarity];

	let endOrientation = {
		x: Math.PI * r[0],
		y: Math.PI * r[1],
		z: Math.PI * r[2]
	};

	let endOrientationEuler = new THREE.Euler( endOrientation.x, endOrientation.y, endOrientation.z );

	let endQuat = new THREE.Quaternion().setFromEuler(endOrientationEuler);
	let startQuat = args.scene.quaternion;

	let t = 0;
	
	timer = setInterval(function() {
		t+=0.01;
		if (t >= 1) {
			clearInterval(timer);
		}

		THREE.Quaternion.slerp( startQuat, endQuat, args.scene.quaternion, t );

	}, 10);

	// Object.assign(args.scene.rotation, endOrientation);

};

const ROT_MAT = [

	// y - forward/backward
	[
		// y - forward
		[

			// z - forward/backward
			[
				// z - forward
				null,
				// z - backward
				null
			],

			// z - right/left
			[
				// z - right
				[ -0.5,  0.0,  -0.5 ],
				// z - left
				[ +0.5,  0.0,  -0.5 ],
			],

			// z - down/up
			[
				// z - down
				[  0.0,  0.0, -0.5 ],
				// z - up
				[  1.0,  0.0, -0.5 ]
			]

		],
		// y - backward
		[

			// z - forward/backward
			[
				// z - forward
				null,
				// z - backward
				null
			],

			// z - right/left
			[
				// z - right
				[ -0.5,  0.0,  +0.5 ],
				// z - left
				[ +0.5,  0.0,  +0.5 ],
			],

			// z - down/up
			[
				// z - down
				[  0.0,  0.0, +0.5 ],
				// z - up
				[  1.0,  0.0, +0.5 ]
			]

		],
	],
	
	// y - right/left
	[
		// y - right (do rotations around y)
		[

			// z - forward/backward
			[
				// z - forward
				[  0.0, +0.5,  0.0 ],
				// z - backward
				[  0.0, -0.5,  0.0 ]
			],

			// z - right/left
			[
				// z - right
				null,
				// z - left
				null
			],

			// z - down/up
			[
				// z - down
				[  0.0,  0.0,  0.0 ],
				// z - up
				[  0.0,  1.0,  0.0 ]
			]

		],
		// y - left (invert y around x, then do rotations around y)
		[

			// z - forward/backward
			[
				// z - forward
				[  1.0, +0.5,  0.0 ],
				// z - backward
				[  1.0, -0.5,  0.0 ]
			],

			// z - right/left
			[
				// z - right
				null,
				// z - left
				null
			],

			// z - down/up
			[
				// z - down
				[  1.0,  1.0,  0.0 ],
				// z - up
				[  1.0,  0.0,  0.0 ]
			]

		],

	],

	// y - down/up (rotate about x to set y, then about y to set z)
	[
		// y - down
		[

			// z - forward/backward
			[
				// z - forward
				[ +0.5, +0.5,  0.0 ],
				// z - backward
				[ +0.5, -0.5,  0.0 ]
			],

			// z - right/left
			[
				// z - right
				[ +0.5,  1.0,  0.0 ],
				// z - left
				[ +0.5,  0.0,  0.0 ]
			],

			// z - down/up
			[
				// z - down
				null,
				// z - up
				null
			]

		],
		// y - up
		[

			// z - forward/backward
			[
				// z - forward
				[ -0.5, +0.5,  0.0 ],
				// z - backward
				[ -0.5, -0.5,  0.0 ]
			],

			// z - right/left
			[
				// z - right
				[ -0.5,  0.0,  0.0 ],
				// z - left
				[ -0.5,  1.0,  0.0 ]
			],

			// z - down/up
			[
				// z - down
				null,
				// z - up
				null
			]

		]

	]

];