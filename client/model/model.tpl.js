const POSITION_SCALE_FACTOR = 0.1;
const COLOR_POSITIVE = 0xcb2127;

const DIM_RT_SCALE = 5;
const DIM_AXLE_LENGTH = 5;
const DIM_AXLE_RADIUS = 0.1;
const DIM_AXLE_WHEEL_RADIUS = 0.3;
const DIM_AXLE_WHEEL_WIDTH = 0.1;



Template.model.onCreated(function() {
    this.showingRearView = new ReactiveVar(true);

    this.autorun(() => {
        updateModelFromConfig( getConfig() );
    });
});

Template.model.onRendered(function() {
    let template = this;

    container = document.createElement('div');
    $('#model').append(container);

    init({
        onOrbit: function() {
            template.showingRearView.set(false);
        }
    });
    animate();
});

Template.model.helpers({
    showingRearView: () => Template.instance().showingRearView.get()
});

Template.model.events({
    'click .js-rear-view': (event, template) => {
        Object.assign(camera.position, CAMERA_DEFAULT.position);
        Template.instance().showingRearView.set(true);
    }
});

// THREE.js stuff

GBL = {};

const SCENE_DEFAULT = {
    rotation: {
        x: Math.PI/2,       // this gets scene into correct orientation for orbit controls to work
        y: 0,
        z: 0

        // x: 0,
        // y: 0,
        // z: 0
    },
    position: {
        x: 0,
        y: -0.5,
        z: 0
    }
};

const CAMERA_DEFAULT = {     // start camera in default view for back view
    position: {
        // x: 0,
        // y: 0,
        // z: 0

        x: -5,
        y: 0.5,
        z: 1.285
    }
};

const RT_OFFSETS = {
    rotation: {
        x: -Math.PI/2,
        y: Math.PI-0.20,
        z: 0.08
    },
    position: {
        x: -1,
        y: 0.2,
        z: 0
    }
};

// if (!Detector.webgl) {
//     Detector.addGetWebGLMessage();
// }

var container;

var camera, controls, scene, rtScene, renderer;
var lighting, ambient, keyLight, fillLight, backLight;

var width = 400, height = 300;

var windowHalfX = width / 2;
var windowHalfY = height / 2;

function init(options) {

    /* Camera */

    camera = new THREE.PerspectiveCamera(45, width / height, 1, 1000);
    Object.assign(camera.position, CAMERA_DEFAULT.position);

    // var helper = new THREE.CameraHelper( camera );
    // scene.add( helper );

    /* Scene */

    scene = new THREE.Scene();
    window.scene = scene;

    lighting = false;

    ambient = new THREE.AmbientLight(0xffffff, 1.0);
    scene.add(ambient);

    keyLight = new THREE.DirectionalLight(new THREE.Color('hsl(30, 100%, 75%)'), 1.0);
    keyLight.position.set(-100, 0, 100);

    fillLight = new THREE.DirectionalLight(new THREE.Color('hsl(240, 100%, 75%)'), 0.75);
    fillLight.position.set(100, 0, 100);

    backLight = new THREE.DirectionalLight(0xffffff, 1.0);
    backLight.position.set(100, 0, -100).normalize();



    /* Background mesh */

    let cubegeometry = new THREE.CubeGeometry(1, 3, 1);
    let cubematerial = new THREE.MeshBasicMaterial({ wireframe: true, color: 0xA5A5A5 });
    let cube = new THREE.Mesh( cubegeometry, cubematerial );
    scene.add( cube );
    cube.position.x = 0;
    cube.position.y = 0;
    cube.position.z = 0;

    /* Model - RT3000 */

    rtScene = new THREE.Scene();        
    var mtlLoader = new THREE.MTLLoader();
    mtlLoader.setBaseUrl('/models/');
    mtlLoader.setPath('/models/');
    mtlLoader.load('RT3000.mtl', function (materials) {

        materials.preload();

        // materials.materials.default.map.magFilter = THREE.NearestFilter;
        // materials.materials.default.map.minFilter = THREE.LinearFilter;

        var objLoader = new THREE.OBJLoader();

        _.each(materials.materials, function(m) {
            m.transparent = true;
            m.opacity = 0.9;
        });

        objLoader.setMaterials(materials);
        objLoader.setPath('/models/');
        objLoader.load('RT3000.obj', function (rt) {        // TODO: find way to cache this?

            Object.assign(rt.rotation, RT_OFFSETS.rotation);
            Object.assign(rt.position, RT_OFFSETS.position);

            rt.scale.set(5,5,5);
            rtScene.add(rt);
            updateModelFromConfig( getConfig() );

        });

    });
    scene.add(rtScene);


        function createCylinder(radius, length) {
            let geometry = new THREE.CylinderGeometry( radius, radius, length, 32 );
            let material = new THREE.MeshBasicMaterial( { color: COLOR_POSITIVE } );
            let cylinder = new THREE.Mesh( geometry, material );
            return cylinder;
        }


    /* Model - axles */

    function createAxleScene() {
        let axleScene = new THREE.Scene();


        let axle = createCylinder( DIM_AXLE_RADIUS, DIM_AXLE_LENGTH );
        let wheelLeft  = createCylinder( DIM_AXLE_WHEEL_RADIUS, DIM_AXLE_WHEEL_WIDTH );
        let wheelRight = createCylinder( DIM_AXLE_WHEEL_RADIUS, DIM_AXLE_WHEEL_WIDTH );


        axleScene.add( axle );
        axleScene.add( wheelLeft );
        axleScene.add( wheelRight );
        // axleScene.add( axle );
        wheelLeft.position.y = DIM_AXLE_LENGTH/2;
        wheelRight.position.y = -DIM_AXLE_LENGTH/2;



        return axleScene;
    }
    scene.add( createAxleScene() );         // rear axle



    /* Model - Vehicle (Humvee) */
    /*
    vScene = new THREE.Scene(); 
    var mtlLoader = new THREE.MTLLoader();
    mtlLoader.setBaseUrl('/models/');
    mtlLoader.setPath('/models/');
    mtlLoader.load('humvee.obj.mtl', function (materials) {

        materials.preload();

        // materials.materials.default.map.magFilter = THREE.NearestFilter;
        // materials.materials.default.map.minFilter = THREE.LinearFilter;

        var objLoader = new THREE.OBJLoader();

        _.each(materials.materials, function(m) {
            m.transparent = true;
            m.opacity = 0.5;
        });

        objLoader.setMaterials(materials);
        objLoader.setPath('/models/');
        objLoader.load('humvee.obj', function (v) {

            // Object.assign(rt.rotation, RT_OFFSETS.rotation);
            // Object.assign(v.position, RT_OFFSETS.position);

            v.scale.set(1,1,1);
            vScene.add(v);

        });

    });
    scene.add(vScene);
    */

    /* Model axes */
    var lineMaterialAxisY = new THREE.LineBasicMaterial({ color: 0x11c1f3 });    
    var lineGeometryAxisY = new THREE.Geometry();
    lineGeometryAxisY.vertices.push(new THREE.Vector3(0, 0, 0));
    lineGeometryAxisY.vertices.push(new THREE.Vector3(0, 1, 0));
    var lineAxisY = new THREE.Line(lineGeometryAxisY, lineMaterialAxisY);

    var lineMaterialAxisZ = new THREE.LineBasicMaterial({ color: 0x33cd5f });    
    var lineGeometryAxisZ = new THREE.Geometry();
    lineGeometryAxisZ.vertices.push(new THREE.Vector3(0, 0, 0));
    lineGeometryAxisZ.vertices.push(new THREE.Vector3(0, 0, 1));
    var lineAxisZ = new THREE.Line(lineGeometryAxisZ, lineMaterialAxisZ);

    rtScene.add(lineAxisY);
    rtScene.add(lineAxisZ);


    // axes
    let axisHelper = new THREE.AxisHelper( 5 );
    // scene.add( axisHelper );

    Object.assign(scene.rotation, SCENE_DEFAULT.rotation);
    Object.assign(scene.position, SCENE_DEFAULT.position);

    /* Renderer */

    renderer = new THREE.WebGLRenderer({ alpha: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(width, height);
    // renderer.setClearColor(new THREE.Color("hsl(100%, 100%, 100%)"));
    // renderer.setClearColor(new THREE.Color("hsl(0, 0%, 10%)"));

    container.appendChild(renderer.domElement);

    /* Controls */

    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.enableDamping = true;
    controls.dampingFactor = 0.25;
    controls.enableZoom = false;
    controls.addEventListener( 'start', function ( event ) {
        if (options && options.onOrbit) options.onOrbit();
    } );

    /* Events */

    window.addEventListener('resize', onWindowResize, false);
    window.addEventListener('keydown', onKeyboardEvent, false);

}

function onWindowResize() {

    windowHalfX = width / 2;
    windowHalfY = height / 2;

    camera.aspect = width / height;
    camera.updateProjectionMatrix();

    renderer.setSize(width, height);

}

function onKeyboardEvent(e) {

    if (e.code === 'KeyL') {

        lighting = !lighting;

        if (lighting) {

            ambient.intensity = 0.25;
            scene.add(keyLight);
            scene.add(fillLight);
            scene.add(backLight);

        } else {

            ambient.intensity = 1.0;
            scene.remove(keyLight);
            scene.remove(fillLight);
            scene.remove(backLight);

        }

    }

}

function animate() {

    requestAnimationFrame(animate);

    controls.update();

    render();

}

function render() {

    renderer.render(scene, camera);

}

const ROTATION_MATRIX_Y = [

    [
        (s)=>{
            s.rotation.x = 0;
            s.rotation.z = (-Math.PI/2);
        }, (s)=>{
            s.rotation.x = 0;
            s.rotation.z = (Math.PI/2);
        }
    ],
    [
        (s)=>{
            s.rotation.x = 0;
            s.rotation.z = (0);
        }, (s)=>{
            s.rotation.x = 0;
            s.rotation.z = (Math.PI);
        }
    ],
    [   
        (s)=>{
            s.rotation.x = (Math.PI);
            s.rotation.z = 0;
        }, (s)=>{
            s.rotation.x = (Math.PI);
            s.rotation.z = 0;
        }
    ]

];

const ROTATION_MATRIX_Z = [

    [
        (s)=>s.rotation.z = (-Math.PI/2), (s)=>s.rotation.z = (Math.PI/2)
    ],
    [
        (s)=>s.rotation.z = (0), (s)=>s.rotation.z = (Math.PI)
    ],
    [   
        (s)=>s.rotation.z = (Math.PI), (s)=>s.rotation.z = (Math.PI)
    ]

];


function updateModelFromConfig(config) {
    updateOrientationFromConfig(config);
    updatePositionFromConfig(config);
}

function updateOrientationFromConfig(config) {

    // orientation
    if (rtScene === undefined) {
        console.log('rtScene undefined');
        return;
    } 

    if (
           ( config.yAxisPair == null || config.yAxisPolarity == null ) 
        && ( config.zAxisPair == null || config.zAxisPolarity == null ) 
    ) {
        rtScene.visible = false;
        return;
    }

    // at least one axis pair and polarity are set
    
    rtScene.visible = true;
    rotateSceneToOrientate({
        scene: rtScene,
        yAxisPair: config.yAxisPair,
        yAxisPolarity: config.yAxisPolarity,
        zAxisPair: config.zAxisPair,
        zAxisPolarity: config.zAxisPolarity
    });

}

function updatePositionFromConfig(config) {

    if (rtScene === undefined) {
        return;
    } 

    // TODO: work out coordinate system
    rtScene.position.x = -config.xPosToFixedAxle*POSITION_SCALE_FACTOR || 0;
    rtScene.position.y =  config.yPosToCentreline*POSITION_SCALE_FACTOR || 0;
    rtScene.position.z = -config.zPosToGround*POSITION_SCALE_FACTOR || 0;

};
