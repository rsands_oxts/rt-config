const SCAN_TIME = 5000; 	// scan for milliseconds
const SCAN_PORT = 3000;		// port to listen


// Dummy COMM object

RTCOMM = {

	getDevices: () => {

		return [

			{
				sn: '1234',
				productFamily: 'RT3000',
				model: 'RT3003Gv2',
				ip: Meteor.settings.public.rtIpAddress,
				model: 'RT3000'
			}

		];

	}

};





// let received = 0;

// // Handle the "onReceive" event.
// var onReceive = function(info) {
// 	// if (received > 30) return;
// 	// received += 1;

//   // console.log(info);
//   // console.log(new Uint8Array(info.data));

// };

// Meteor.startup(function() {
// 	let socketId;

// 	// Create the Socket
// 	chrome.sockets.udp.create({}, function(socketInfo) {

// 	  socketId = socketInfo.socketId;
// 	  console.log('socket opened', socketId);

// 	  // Setup event handler and bind socket.

// 	  chrome.sockets.udp.onReceive.addListener(onReceive);

// 	  chrome.sockets.udp.bind(socketId,
// 	    "0.0.0.0", SCAN_PORT, function(result) {
// 	      if (result < 0) {
// 	        console.log("Error binding socket.");
// 	        return;
// 	      }
// 	      console.log('listening for udp :) :)', socketId, result);

// 	      // chrome.sockets.udp.send(socketId, arrayBuffer,
// 	      //   '127.0.0.1', 1337, function(sendInfo) {
// 	      //     console.log("sent " + sendInfo.bytesSent);
// 	      // });
// 	  });

// 	  setTimeout(function() {
// 			// close the socket
// 			chrome.sockets.udp.close(socketId, function() {
// 				console.log('socket closed', socketId);
// 			});
// 	  }, SCAN_TIME);
// 	});

// });

