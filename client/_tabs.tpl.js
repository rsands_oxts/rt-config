Template._tabs.onCreated(function() {

    Session.set('ionTab.current', 'tabs.status');
    Tracker.autorun(function() {
        let tab = Session.get("ionTab.current");
        console.log("tab change", tab);
        
        switch (tab) {
            case 'tabs.status':
                FlowRouter.go('analysisOverview');
                break;
            case 'tabs.measurements':
                FlowRouter.go('measurementsOverview');
                break;
            case 'tabs.config':
                FlowRouter.go('configOverview');
                break;
        }
    });
});