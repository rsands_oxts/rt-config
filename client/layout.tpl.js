// Global functions

getConfig = (key) => {
	let version = Versions.findOne( Session.get('versionId') );
	if (!version) return;

	return (key) ? version.config[key] : version.config;
};
setConfig = (key, value) => {
	let version = Versions.findOne( Session.get('versionId') );
	if (!version) return;

	// TODO: check to start new draft if current version has been committed

	let setModifier = {}
	setModifier[`config.${key}`] = value;

	Versions.update(Session.get('versionId'), { $set: setModifier });
};
saveDraftIfChanged = () => {
	// TODO: no longer needed ?
	let config = Session.get('config');
	if (!config) return;

	let changed = Session.get('currentVersionChanged');
	if (!changed) return;

	let currentVersionId = Session.get('currentVersionId');

	if (currentVersionId && Configs.findOne(currentVersionId)) {
		createVersion();
	} else {
		currentVersionId = Setups.insert({});
		createVersion()
	}

	function createVersion() {
		currentVersionId = Versions.insert();
		Session.set('currentVersionId', currentVersionId);
	}
};

Template.layout.onCreated(function() {

	this.subscribe('setups.all');
	this.subscribe('versions.all');

	this.autorun(() => {
		if ( this.subscriptionsReady() ) {
			let latestVersion = Versions.findOne({}, { sort: { _updatedAt: -1 } });		// this should never be null
			Session.set('versionId', latestVersion._id);
		}
	});

	this.states = [
		'configOverview',
		'orientation',
		'position'
	];

	this.stateIndex = 0;

});

Template.layout.onRendered(function() {
	
  IonSideMenu.snapper.settings({touchToDrag: false});
  if (Meteor.isCordova) navigator.splashscreen.hide();

  // debugger;
  // $('*[data-momentum=""]').preprend('<h1></h1>');

});

Template.layout.helpers({

	setups() {
		return Setups.find()		// TODO: order
	},

	version() {
		return Versions.findOne( Session.get('versionId') );
	},

	config(key) {
		return getConfig(key);
	}

});

Template.layout.events({

	'click .js-setup': function(event, template) {
		let setup = Setups.findOne(event.currentTarget.dataset.setupId);
		let version = setup.latestVersion();
		Session.set('versionId', version._id);
	},

	'click .js-next': function(event, template) {
		IonNavigation.skipTransitions = true;
		$('.content').find().addClass('has-footer');

		template.stateIndex += 1;
		if (template.stateIndex > template.states.length-1) template.stateIndex = template.states.length-1;
		FlowRouter.go(template.states[template.stateIndex], { configId: FlowRouter.getParam('configId') });		// TODO: versionId ?
	},

	'click .js-back': function(event, template) {
		IonNavigation.skipTransitions = true;
		$('.content').find().addClass('has-footer');

		template.stateIndex -= 1;
		if (template.stateIndex < 0) template.stateIndex = 0;
		FlowRouter.go(template.states[template.stateIndex], { configId: FlowRouter.getParam('configId') });
	}

});

