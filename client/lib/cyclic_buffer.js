CyclicBuffer = function(size) {
    this.size = size || 500;

    this.current = -1;
    this.data = [];

    this.add = (elem) => {
        // increment current
        let index = ++this.current;
        // delete old object
        delete this.data[index];
        // save new element
        this.data[index] = elem;
        // cycle current
        if ( this.current === this.size-1 )
            this.current = -1;

        return elem;
    };

    this.getCurrent = () => {
        let index = (this.current > -1) ? this.current : this.size-1;
        return this.data[index];
    };

    this.getAll = () => {
        let index = (this.current+1);
        let result = [];
        let elem;
        for(let j=0;j<this.size;j++){
            elem = this.data[(index+j)%this.size]
            if (elem) result.push( elem );
        }
        return result;
    };

    this.reset = () => {
        this.current = 0;
        this.data = [];
    };
};