Template._ncomGraph.onCreated(function() {
	// this.startTimeMs = NcomService.hiresBuffer.getCurrent()._gpsTimeMs;
});

Template._ncomGraph.onRendered(function() {
	let template = this;

	let datasets = [];
	template.data.ncomKeys.forEach((ncomKey,index) => {
		datasets.push({
			label: ncomKey,
			data: [],
			fill: false,
			pointRadius: 0,
			backgroundColor: 'transparent',
			borderColor: (template.data.ncomColours && template.data.ncomColours[index]) || 'black'		
		});
	});

	let ctx = template.find('.graph-ctx');// document.getElementById("_ncomGraphCtx");
	template.lineChart = new Chart(ctx, {
	    type: 'line',
	    data: {
	        datasets: datasets
	    },
	    options: {
	        scales: {
	            xAxes: [{
					display: false,
					ticks: {
						stepSize: 1000,
						callback: function(value, index, values) {
							return '|' // value/1000;
						}
					},
					// display: false,
	                type: 'linear',
	                position: 'bottom'
				}],
				yAxes: [{
					display: false,
					ticks: {
						min: -20,
						max: 10	
					}
				}]
			},
			animation: {
				duration: 0
			},
			legend: {
				display: false
			}
	    }
	});
	
	// update graph
    Meteor.setInterval(function() {
		ncomBuffer = NcomService.hiresBuffer.getAll();

		template.data.ncomKeys.forEach((ncomKey,index) => {

			template.lineChart.data.datasets[index].data = _.map(ncomBuffer, (ncom) => {
				return { x: ncom._gpsTimeMs, y: ncom[ncomKey] }
			});	
			
		});

		template.lineChart.update();
    }, 100)
    
});

Template._ncomGraph.helpers({

});