// TODO: better way to get data context of template (instead of dims functions)?
// TODO: format amount config

const TICK_WIDTH = 40;
const SLIDER_HEIGHT = 40;
const TICK_TEXT_OFFSET_X = -5;

const DEFAULT_MIN = 0;
const DEFAULT_MAX = 100;
const DEFAULT_STEP = 1;

const DEFAULT_VALUE = 0;

let ClosestSlider = (template) => template || Template.instance().closest('_slider');

let dims = {

	min:  (template) => ClosestSlider(template).data.min  || DEFAULT_MIN,		// would be nice to use currentData(), but this breaks nested templates
	max:  (template) => ClosestSlider(template).data.max  || DEFAULT_MAX,
	step: (template) => ClosestSlider(template).data.step || DEFAULT_STEP,

	width: (template) => TICK_WIDTH * ( dims.max(template) - dims.min(template) ),

	elemWidth: (template) => ClosestSlider(template).elementWidth.get(),
	zeroOffsetX: (template) => ( -TICK_WIDTH*dims.min(template) + dims.elemWidth(template)/2 ),

	xForValue: (value, template) => {

		let min = dims.min(template);
		return ( value - min ) * TICK_WIDTH;

	}

};

Template._slider.onCreated(function() {
	this.value = new ReactiveVar(DEFAULT_VALUE);		// initial value
	this.elementWidth = new ReactiveVar(null);
});

Template._slider.onRendered(function() {

	let template = this;
	let $scrollContainerElem = $( template.find('.scroll-slider-scroll') )

	let $sliderElement = $(template.find('.scroll-slider'));
	let sliderEventSnap = 	$.Event('slider-snap');
	let sliderEventScroll = $.Event('slider-scroll');

	// this could update if width changes (to support dynamic responsive principles)

	template.elementWidth.set( $scrollContainerElem.width() );

	// scroll to current value

	$scrollContainerElem.scrollLeft( dims.xForValue( template.value.get(), template ) );
	// scroll event

	let unsnappedValue = null;
	let snappedValue = null, lastSnappedValue = null;
	let t;
	$scrollContainerElem.on('scroll', function(event) {
		if (t) clearTimeout(t);

		let step = dims.step(template);

		unsnappedValue = dims.min(template) + event.target.scrollLeft / TICK_WIDTH;
		template.value.set(unsnappedValue.toFixed(1));
		$sliderElement.trigger(sliderEventScroll, unsnappedValue);

		snappedValue = ( Math.round(unsnappedValue/step) ) * step;
		template.value.set(snappedValue);
		if ( lastSnappedValue !== snappedValue ) {
			$sliderElement.trigger(sliderEventSnap, snappedValue);
			lastSnappedValue = snappedValue;
		}

		t = setTimeout(function() {
			let x = dims.xForValue( snappedValue, template );
			$scrollContainerElem.animate({scrollLeft:x}, '100', );
		}, 100);
	});


});

Template._slider.helpers({

	ticks: () => {

		let ticks = [];
		let marginTicksCount = Math.ceil( ( dims.elemWidth() / 2 ) / TICK_WIDTH );
		let min=dims.min()-marginTicksCount, max=dims.max()+marginTicksCount;

		for ( let i=min; i<max; i++ ) {
			ticks.push(i);
		}
		return ticks;

	},

	tickStyleStr: (tick) => ( tick < dims.min() || tick >= dims.max() ) ? "opacity: 0.2;" : "",
	tickMainStyleStr: (tick) => ( tick < dims.min() || tick > dims.max() ) ? "opacity: 0.2;" : "",	// this allows for upper bound

	tickText: (tick) => ( tick<0 && ClosestSlider().data.hideMinus ) ? `${-(tick)}` : `${(tick)}`,

	pxWidth: () => dims.width() + dims.elemWidth(),		// adding elemWidth allows for margin ticks
	pxHeight: () => SLIDER_HEIGHT,

	pxTickMainX: (tick) =>      dims.zeroOffsetX() + (tick * TICK_WIDTH),
	pxTickHalfX: (tick) =>      dims.zeroOffsetX() + (tick * TICK_WIDTH) + TICK_WIDTH/2,
	pxTickDecX:  (tick, dec) => dims.zeroOffsetX() + (tick * TICK_WIDTH) + TICK_WIDTH*dec/10,
	pxTickTextX:  (tick) =>     dims.zeroOffsetX() + (tick * TICK_WIDTH) + TICK_TEXT_OFFSET_X,

	pxTickMainY: () =>  SLIDER_HEIGHT * 30/50,
	pxTickHalfY: () =>  SLIDER_HEIGHT * 20/50,
	pxTickDecY:  () =>  SLIDER_HEIGHT * 15/50,
	pxTickTextY:  () => SLIDER_HEIGHT * 49/50,

	value: () => Template.instance().value.get(),

	positiveStr: () => ClosestSlider().data.positive,
	negativeStr: () => ClosestSlider().data.negative,
	zeroStr:     () => ClosestSlider().data.zero,


});