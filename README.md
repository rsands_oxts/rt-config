# rtconfig

### Getting started

Clone this project and cd into project folder:

```
git clone https://github.com/OxfordTechnicalSolutions/rtconfig.git
cd rtconfig
```

Run webserver:
```
meteor --settings settings.json
```

Navigate to http://localhost:3000
